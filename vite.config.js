import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import * as path from 'path'
import AutoImport from 'unplugin-auto-import/vite'
import Components from 'unplugin-vue-components/vite'
import viteCompression from 'vite-plugin-compression'

export default defineConfig({
  plugins: [
    vue(),
    AutoImport({
      imports: ['vue', 'vue-router', 'pinia']
    }),
    Components({
      // 配置需要自动注册的组件
      dts: true,
      dirs: ['src/components'],
      extensions: ['vue'],
      deep: true,
      resolvers: [],
      directoryAsNamespace: false // 允许子目录作为组件的命名空间前缀。
    }),
    // 代码压缩
    viteCompression({
      verbose: true, // 是否在控制台输出压缩结果
      disable: false, // 是否禁用压缩
      threshold: 10240, // 启用压缩的文件大小限制，单位是字节
      deleteOriginFile: false, // 是否删除压缩源文件
      algorithm: 'gzip',
      ext: '.gz'
    })
  ],
  resolve: {
    alias: {
      '@': path.join(__dirname, '/src')
    }
  },
  server: {
    port: 9527,
    open: true,
    proxy: {
      '/api': {
        // 本地服务接口地址
        target: 'http://192.168.9.230:18056/api', // 测试环境
        changeOrigin: true,
        rewrite: (path) => path.replace(/^\/api/, '')
      }
    }
  }
})
