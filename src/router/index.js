import {
  createRouter,
  createWebHashHistory,
  createWebHistory
} from 'vue-router'
import routes from './routes'
import website from '@/config/website'
import { getToken } from '@/util/auth'
import { tagStore } from '@/store/tag'
import WujieVue from 'wujie-vue3'
const { bus } = WujieVue

// console.log(routes)

const scrollBehavior = function scrollBehavior(to, from, savedPosition) {
  if (savedPosition) {
    return savedPosition
  } else {
    return { x: 0, y: 0 }
  }
}

const router = createRouter({
  history: createWebHashHistory('/'), // hash模式：createWebHashHistory
  // history: createWebHistory('/'), // history模式：createWebHistory
  routes,
  scrollBehavior
})

router.beforeEach((to, form, next) => {
  // to表示将要访问的路径
  // form表示从那个页面跳转而来
  // next表示允许跳转到指定位置
  if (to.path === website.loginUrl || to.meta.isAuth === false) {
    return next()
  }
  // 获取用户本地的token, 如果token不存在则跳转到登录页
  if (!getToken()) {
    return next({
      name: 'login'
    })
  }

  if (to.meta.title) {
    document.title = `${website.title} - ${to.meta.title}`
    if (to.meta.isTab !== false) {
      const store = tagStore()
      store.ADD_TAG(to)
    }
  }

  next()
})

export default router
