import common from './common'
import { viewsRoute } from './viewsRoute'

export default [
  ...common, // 公共路由
  ...viewsRoute() // views 路由
]
