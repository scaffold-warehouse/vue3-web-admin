import { getRoutes } from './autoRoute'

export const viewsRoute = () => {
  // 获取page.js路径
  const pages = import.meta.glob('../views/**/page.js', {
    eager: true,
    import: 'default'
  })

  // 获取index.vue路径
  const pageComps = import.meta.glob('../views/**/index.vue')

  // 文件夹路径
  const fileName = '../views'

  // 构造路由配置
  const routes = getRoutes(pages, pageComps, fileName)
  return routes
}
