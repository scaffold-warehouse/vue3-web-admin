import Layout from '@/layout/index.vue'
import FullLayout from '@/layout/fullLayout.vue'

export const getRoutes = (pages, pageComps, fileName) => {
  const routes = Object.entries(pages).map(([path, meta]) => {
    const pageJs = path
    path = pageJs.replace(fileName, '').replace('/page.js', '')
    path = path || '/'
    const comPath = pageJs.replace('page.js', 'index.vue')
    return {
      path,
      name: meta.name || meta.title,
      component: meta.fullScreen ? FullLayout : Layout,
      children: [
        {
          path: '',
          name: meta.name || meta.title,
          component: pageComps[comPath],
          meta
        }
      ]
    }
  })
  return routes
}
