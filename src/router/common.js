import Layout from '@/layout/index.vue'

export default [
  {
    path: '/',
    redirect: '/index'
  },
  {
    path: '/login',
    name: 'login',
    component: () => import('@/pages/login.vue')
  },
  {
    path: '/404',
    name: '404',
    component: () => import('@/pages/404.vue')
  },
  {
    path: '/wujie/',
    name: 'wujie',
    component: Layout,
    children: [
      {
        path: ':pathMatch(.*)',
        component: () => import('@/pages/wujie.vue')
      }
    ]
  },
  {
    path: '/:pathMatch(.*)',
    redirect: '/404'
  }
]
