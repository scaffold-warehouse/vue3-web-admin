import { defineStore } from 'pinia'
import { setStore, getStore } from '@/util/store'
import { deepClone } from '@/util/util'

import website from '@/config/website'
import router from '@/router'

export const tagStore = defineStore('tag', () => {
  const tagList = ref(getStore({ name: 'tagList' }) || [])
  const tag = ref(getStore({ name: 'tag' }) || website.firstPage)

  function ADD_TAG(t) {
    // 新增tag
    t.matched.forEach((matched) => (matched.instances = {})) // 解除循环引用
    tag.value = deepClone(t) // 实现深拷贝，解除循环引用
    tag.value.label = t.name || t.meta.title
    tag.value.close = true

    if (tagList.value.some((ele) => ele.path === t.path)) return
    tagList.value.push(tag.value)

    CURRENT_TAG(tag.value)
  }
  function REMOVE_TAG(index) {
    // 删除tag
    tagList.value.splice(index, 1)
    tag.value =
      tagList.value.length > 1
        ? tagList.value[tagList.value.length - 1]
        : website.firstPage
    SAVE_TAG()
    router.push(tag.value.path)
  }
  function CURRENT_TAG(t) {
    // 当前选中tag
    tag.value = t
    SAVE_TAG()
  }
  function CLEAR_OTHER_TAG() {
    // 关闭其他tag
    tagList.value = [tag.value]
    SAVE_TAG()
  }
  function CLEAR_TAG() {
    // 清空
    tag.value = website.firstPage
    tagList.value = []

    SAVE_TAG()
    router.push(website.firstPage.path)
  }
  function SAVE_TAG() {
    setStore({
      name: 'tag',
      content: tag.value,
      type: 'session'
    })
    setStore({
      name: 'tagList',
      content: tagList.value,
      type: 'session'
    })
  }

  return {
    tagList,
    tag,
    ADD_TAG,
    REMOVE_TAG,
    CURRENT_TAG,
    CLEAR_OTHER_TAG,
    CLEAR_TAG
  }
})
