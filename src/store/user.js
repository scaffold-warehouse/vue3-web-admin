import { defineStore } from 'pinia'
import { setStore, getStore, clearStore, removeStore } from '@/util/store'
import common from '@/api/common'
import router from '@/router'
import {
  getToken,
  setToken,
  setRefreshToken,
  getRefreshToken,
  removeToken,
  removeRefreshToken
} from '@/util/auth'
// import website from '@/config/website'

export const userStore = defineStore('user', () => {
  const timer = ref('') // 刷新token
  const tenantId = ref(getStore({ name: 'tenantId' }) || '')
  const userInfo = ref(getStore({ name: 'userInfo' }) || '')
  const permission = ref(getStore({ name: 'permission' }) || '')
  const menu = ref(getStore({ name: 'menu' }) || '')
  const token = ref(getToken() || '')
  const refresh_token = ref(getRefreshToken() || '')
  const dictList = ref(getStore({ name: 'dictList' }) || '')

  function loginByUsername(form, headers) {
    // 登录
    return new Promise((resolve, reject) => {
      common.login
        .loginByUsername(form, headers)
        .then((data) => {
          if (data.access_token) {
            userInfo.value = data
            tenantId.value = data.tenant_id
            token.value = data.access_token
            refresh_token.value = data.refresh_token

            setToken(data.access_token)
            setRefreshToken(data.refresh_token)
            setStore({ name: 'userInfo', content: data })
            setStore({ name: 'tenantId', content: data.tenant_id })
            resolve(data)
          } else {
            reject(data)
          }
        })
        .catch((error) => {
          reject(error)
        })
    })
  }
  function refreshToken() {
    // 刷新token
    window.console.log('handle refresh token')
    return new Promise((resolve, reject) => {
      common.login
        .refreshToken(refresh_token.value, tenantId.value)
        .then((data) => {
          token.value = data.access_token
          refresh_token.value = data.refresh_token

          setToken(data.access_token)
          setRefreshToken(data.refresh_token)
          resolve()
        })
        .catch((error) => {
          reject(error)
        })
    })
  }
  function getPermission() {
    // 获取按钮权限
    common.permission.getButtons().then((res) => {
      let result = []
      function getCode(list) {
        list.forEach((ele) => {
          if (typeof ele === 'object') {
            const chiildren = ele.children
            const code = ele.code
            if (chiildren) {
              getCode(chiildren)
            } else {
              result.push(code)
            }
          }
        })
      }

      getCode(res.data)
      permission.value = {}
      result.forEach((ele) => {
        permission.value[ele] = true
      })
      setStore({
        name: 'permission',
        content: permission.value,
        type: 'session'
      })
    })
  }
  function getMenuAll() {
    // 获取菜单
    return new Promise((resolve) => {
      common.permission.getRoutes().then((res) => {
        setStore({
          name: 'menu',
          content: res.data
        })
        menu.value = res.data
        resolve(res.data)
        // console.log(menu)
      })
    })
  }
  function getDict() {
    // 获取字典
    common.permission.getDicTreeAll().then((res) => {
      setStore({
        name: 'dictList',
        content: res.data
      })
    })
  }
  function LogOut() {
    // 注销、退出登录
    removeToken()
    removeRefreshToken()
    removeStore({ name: 'userInfo' })
    removeStore({ name: 'menu' })
    removeStore({ name: 'tenantId' })
    clearStore({ type: 'session' })
    router.push({ name: 'login' })
  }
  function setIntervalToken() {
    // 每10分钟更新一次token
    timer.value = window.setInterval(
      () => {
        if (!getRefreshToken()) return
        refreshToken()
      },
      1000 * 60 * 10
    )
  }

  return {
    tenantId,
    userInfo,
    permission,
    menu,
    token,
    refresh_token,
    dictList,
    loginByUsername,
    refreshToken,
    getPermission,
    getMenuAll,
    getDict,
    LogOut,
    setIntervalToken
  }
})
