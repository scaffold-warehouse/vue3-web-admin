/**
 * 全局配置文件
 */

export default {
  bNoPlugin: 1, // 是否支持无插件播放视频,1不支持 2支持
  title: '后台管理系统',
  logo: '/img/logo.png',
  key: 'v3', // 配置主键,目前用于存储
  loginTitle: '后台管理系统',
  clientId: import.meta.env.VITE_CLIENT_ID, // 客户端id
  clientSecret: import.meta.env.VITE_CLIENT_SECRET, // 客户端密钥
  tenantMode: false, // 是否开启租户输入模式
  tenantId: '000000', // 管理组租户编号
  captchaMode: process.env.NODE_ENV === 'production', // 是否开启验证码模式(生产模式开启，开发模式不开启)
  tokenTime: 1800, // 单位秒，30分钟更新一次token
  loginUrl: '/login', // 登录页地址
  tokenHeader: 'Blade-Auth',
  imgPath: '', // 图片前缀
  firstPage: {
    name: 'index',
    label: '首页',
    path: '/index',
    id: '0',
    close: false
  },
  dictProps: {
    // 配置字典属性
    label: 'dictValue',
    value: 'dictKey'
  },
  childPath: 'http://192.168.9.230:18056/sub_packages', // 子应用路径，末尾不带/
  childPrefix: '/wujie', // 子应用路由前缀
  sm2Key:
    '04b7cee4ae36faf88bac1ff557524dec1575cb745c3a888f21e80bc9035189b33e6f40c79d32a98d4128632f051abde21daf12463fabcc7a665a33427f63e59137' // sm2加密公钥
}
