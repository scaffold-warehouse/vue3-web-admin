import { validatenull } from './validate'

//表单序列化
export const serialize = (data) => {
  let list = []
  Object.keys(data).forEach((ele) => {
    list.push(`${ele}=${data[ele]}`)
  })
  return list.join('&')
}

export const getObjType = (obj) => {
  var toString = Object.prototype.toString
  var map = {
    '[object Boolean]': 'boolean',
    '[object Number]': 'number',
    '[object String]': 'string',
    '[object Function]': 'function',
    '[object Array]': 'array',
    '[object Date]': 'date',
    '[object RegExp]': 'regExp',
    '[object Undefined]': 'undefined',
    '[object Null]': 'null',
    '[object Object]': 'object'
  }
  if (obj instanceof Element) {
    return 'element'
  }
  return map[toString.call(obj)]
}
export const getViewDom = () => {
  return window.document
    .getElementById('avue-view')
    .getElementsByClassName('el-scrollbar__wrap')[0]
}

/**
 * 对象深拷贝
 */
export const deepClone = (data) => {
  var type = getObjType(data)
  var obj
  if (type === 'array') {
    obj = []
  } else if (type === 'object') {
    obj = {}
  } else {
    //不再具有下一层次
    return data
  }
  if (type === 'array') {
    for (var i = 0, len = data.length; i < len; i++) {
      obj.push(deepClone(data[i]))
    }
  } else if (type === 'object') {
    for (var key in data) {
      obj[key] = deepClone(data[key])
    }
  }
  return obj
}

/**
 * 设置主题
 */
export const setTheme = (name) => {
  document.body.className = name
}

/**
 * 加密处理
 */
export const encryption = (params) => {
  let { data, type, param, key } = params
  let result = JSON.parse(JSON.stringify(data))
  if (type == 'Base64') {
    param.forEach((ele) => {
      result[ele] = btoa(result[ele])
    })
  } else if (type == 'Aes') {
    param.forEach((ele) => {
      result[ele] = window.CryptoJS.AES.encrypt(result[ele], key).toString()
    })
  }
  return result
}

/**
 * 递归寻找子类的父类
 */
export const findParent = (menu, id) => {
  for (let i = 0; i < menu.length; i++) {
    if (menu[i].children.length != 0) {
      for (let j = 0; j < menu[i].children.length; j++) {
        if (menu[i].children[j].id == id) {
          return menu[i]
        } else {
          if (menu[i].children[j].children.length != 0) {
            return findParent(menu[i].children[j].children, id)
          }
        }
      }
    }
  }
}

/**
 * 动态插入css
 */
export const loadStyle = (url) => {
  const link = document.createElement('link')
  link.type = 'text/css'
  link.rel = 'stylesheet'
  link.href = url
  const head = document.getElementsByTagName('head')[0]
  head.appendChild(link)
}

/**
 * 动态插入JS
 */
export const loadJS = (url, callback) => {
  let script = document.createElement('script')
  let fn = callback || function () {}
  script.type = 'text/javascript'
  //IE
  if (script.readyState) {
    script.onreadystatechange = function () {
      if (script.readyState == 'loaded' || script.readyState == 'complete') {
        script.onreadystatechange = null
        fn()
      }
    }
  } else {
    //其他浏览器
    script.onload = function () {
      fn()
    }
  }
  script.src = url
  document.getElementsByTagName('head')[0].appendChild(script)
}

/**
 * 根据字典的value显示label
 */
export const findByvalue = (dic, value) => {
  let result = ''
  if (validatenull(dic)) return value
  if (
    typeof value == 'string' ||
    typeof value == 'number' ||
    typeof value == 'boolean'
  ) {
    let index = 0
    index = findArray(dic, value)
    if (index != -1) {
      result = dic[index].label
    } else {
      result = value
    }
  } else if (value instanceof Array) {
    result = []
    let index = 0
    value.forEach((ele) => {
      index = findArray(dic, ele)
      if (index != -1) {
        result.push(dic[index].label)
      } else {
        result.push(value)
      }
    })
    result = result.toString()
  }
  return result
}

/**
 * 根据字典的value查找对应的index
 */
export const findArray = (dic, value) => {
  for (let i = 0; i < dic.length; i++) {
    if (dic[i].value == value) {
      return i
    }
  }
  return -1
}

/**
 * 生成随机len位数字
 */
export const randomLenNum = (len, date) => {
  let random = ''
  random = Math.ceil(Math.random() * 100000000000000)
    .toString()
    .substr(0, len ? len : 4)
  if (date) random = random + Date.now()
  return random
}

/**
 * 打开小窗口
 */
export const openWindow = (url, title, w, h) => {
  // Fixes dual-screen position                            Most browsers       Firefox
  const dualScreenLeft =
    window.screenLeft !== undefined ? window.screenLeft : screen.left
  const dualScreenTop =
    window.screenTop !== undefined ? window.screenTop : screen.top

  const width = window.innerWidth
    ? window.innerWidth
    : document.documentElement.clientWidth
      ? document.documentElement.clientWidth
      : screen.width
  const height = window.innerHeight
    ? window.innerHeight
    : document.documentElement.clientHeight
      ? document.documentElement.clientHeight
      : screen.height

  const left = width / 2 - w / 2 + dualScreenLeft
  const top = height / 2 - h / 2 + dualScreenTop
  const newWindow = window.open(
    url,
    title,
    'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=yes, copyhistory=no, width=' +
      w +
      ', height=' +
      h +
      ', top=' +
      top +
      ', left=' +
      left
  )

  // Puts focus on the newWindow
  if (window.focus) {
    newWindow.focus()
  }
}

/**
 * 获取顶部地址栏地址
 */
export const getTopUrl = () => {
  return window.location.href.split('/#/')[0]
}

/**
 * 获取url参数
 * @param name 参数名
 */
export const getQueryString = (name) => {
  let reg = new RegExp('(^|&)' + name + '=([^&]*)(&|$)', 'i')
  let r = window.location.search.substr(1).match(reg)
  if (r != null) return unescape(decodeURI(r[2]))
  return null
}

/**
 * 将对象转为URL参数
 * @param param 对象
 */
export function urlEncode(param, key, encode) {
  if (param == null) return ''
  let paramStr = ''
  let t = typeof param
  if (t == 'string' || t == 'number' || t == 'boolean') {
    // if(t == 'string'){
    //   param = param.replace(/\s+/g,'+')
    // }
    // paramStr += '&' + key + '=' + ((encode==null||encode) ? encodeURIComponent(param) : param)
    paramStr += '&' + key + '=' + param
  } else {
    for (let i in param) {
      let k =
        key == null
          ? i
          : key + (param instanceof Array ? '[' + i + ']' : '.' + i)
      paramStr += urlEncode(param[i], k, encode)
    }
  }
  return paramStr
}

/***
 * 防抖
 * func 输入完成的回调函数
 * delay 延迟时间
 */
export function debounce(func, delay) {
  let timer
  return (...args) => {
    if (timer) {
      clearTimeout(timer)
    }
    timer = setTimeout(() => {
      func.apply(this, args)
    }, delay)
  }
}

/**
 * 节流
 * fn 输入完成的回调函数
 * time 延迟时间
 */
export function throttle(fn, time) {
  let flag = true
  return function (...argu) {
    if (!flag) {
      return
    }
    flag = false
    fn(...argu)
    let timer = setTimeout(() => {
      flag = true
      clearTimeout(timer)
      timer = null
    }, time)
  }
}

/**
 * 递归遍历把null转换为''
 */
export function null2str(data) {
  if (typeof data != 'object' || data === null || data === 'null') {
    data = ''
    return data
  } else {
    for (let x in data) {
      if (data[x] === 0) {
        data[x] = '0'
      }
      if (data[x] === null || data[x] === 'null') {
        data[x] = ''
      } else {
        if (Array.isArray(data[x])) {
          data[x] = data[x].map((z) => {
            return null2str(z)
          })
        }
        if (typeof data[x] === 'object') {
          data[x] = null2str(data[x])
        }
      }
    }
    return data
  }
}

/**
 * 获取浏览器类型
 */
export function getBrowserType() {
  let ua = navigator.userAgent
  //ie
  let isIE = ua.indexOf('compatible') > -1 && ua.indexOf('MSIE') > -1
  let isIE11 = ua.indexOf('Trident') > -1 && ua.indexOf('rv:11.0') > -1
  if (isIE || isIE11) {
    return 'IE'
  }
  //firefox
  else if (ua.indexOf('Firefox') >= 0) {
    return 'Firefox'
  }
  //Chrome
  else if (ua.indexOf('Chrome') >= 0) {
    return 'Chrome'
  }
  //Opera
  else if (ua.indexOf('Opera') >= 0) {
    return 'Opera'
  }
  //Safari
  else if (ua.indexOf('Safari') >= 0) {
    return 'Safari'
  }
}

/**
 * 替换null 数据
 */
export function formateData(val, newVal = '-') {
  return val == null || val == undefined ? newVal : val
}

//根据文件链接获取文件名称
export function getFilenameFromUrl(url) {
  const pathname = new URL(url).pathname
  const index = pathname.lastIndexOf('/')
  return -1 !== index ? pathname.substring(index + 1) : pathname
}

/**
 * 将对象中null转为undefined（数字类型显示为空）
 * @param obj 对象
 */
export function resetNull(obj) {
  Object.keys(obj).forEach((item) => {
    if (!obj[item] && obj[item] !== 0) obj[item] = undefined
  })
  return obj
}

/**
 * 获取文件类型
 * @param url 路径
 */
export function getFileType(url) {
  url = url.split('?AccessToken')
  url = url[0]
  let index = url.lastIndexOf('.')
  let ext = url.substr(index + 1)
  let typeList = ['image', 'audio', 'video', 'pdf', 'doc', 'excel', 'file']
  let length = typeList.length - 1
  let suffixJson = {
    image: [
      'png',
      'jpg',
      'jpeg',
      'gif',
      'ico',
      'bmp',
      'pic',
      'tif',
      'wbmp',
      'jp2',
      'tiff',
      'gfif',
      'exif',
      'mbm',
      'jfif',
      'webp'
    ],
    audio: ['mp3', 'ogg', 'wav', 'm4a', 'acc', 'vorbis', 'silk'],
    video: [
      'mp4',
      'webm',
      'avi',
      'rmvb',
      '3gp',
      '3pg',
      'flv',
      'mov',
      'mpeg',
      'mpg',
      'dat',
      'asf',
      'wmv',
      'navi',
      'deo',
      'mkv',
      'hddvd',
      'qsv',
      'ogv'
    ],
    pdf: ['pdf'],
    doc: ['doc', 'docx'],
    excel: ['xls', 'xlsx']
  }
  let resultList = []
  for (let attr in suffixJson) {
    resultList.push(suffixJson[attr].indexOf(ext.toLowerCase()))
  }
  let posIndex = resultList.map((n) => n > -1).indexOf(true)
  return posIndex != -1 ? typeList[posIndex] : typeList[length]
}
