/**
 * 计算时间差
 * date1 开始时间
 * date2 结束时间
 */
export const calcDate = (date1, date2) => {
  let date3 = date2 - date1

  let days = Math.floor(date3 / (24 * 3600 * 1000))

  let leave1 = date3 % (24 * 3600 * 1000) //计算天数后剩余的毫秒数
  let hours = Math.floor(leave1 / (3600 * 1000))

  let leave2 = leave1 % (3600 * 1000) //计算小时数后剩余的毫秒数
  let minutes = Math.floor(leave2 / (60 * 1000))

  let leave3 = leave2 % (60 * 1000) //计算分钟数后剩余的毫秒数
  let seconds = Math.round(date3 / 1000)
  return {
    leave1,
    leave2,
    leave3,
    days: days,
    hours: hours,
    minutes: minutes,
    seconds: seconds
  }
}

/**
 * 日期格式化
 */
export function dateFormat(date, format) {
  format = format || 'yyyy-MM-dd hh:mm:ss'
  if (date !== 'Invalid Date') {
    let o = {
      'M+': date.getMonth() + 1, //month
      'd+': date.getDate(), //day
      'h+': date.getHours(), //hour
      'm+': date.getMinutes(), //minute
      's+': date.getSeconds(), //second
      'q+': Math.floor((date.getMonth() + 3) / 3), //quarter
      S: date.getMilliseconds() //millisecond
    }
    if (/(y+)/.test(format))
      format = format.replace(
        RegExp.$1,
        (date.getFullYear() + '').substr(4 - RegExp.$1.length)
      )
    for (let k in o)
      if (new RegExp('(' + k + ')').test(format))
        format = format.replace(
          RegExp.$1,
          RegExp.$1.length === 1
            ? o[k]
            : ('00' + o[k]).substr(('' + o[k]).length)
        )
    return format
  }
  return ''
}

/**
 * 上一天/下一天
 */
export function getNextDate(date, day) {
  let dd = date ? new Date(date) : new Date()
  dd.setDate(dd.getDate() + day)
  let y = dd.getFullYear()
  let m = dd.getMonth() + 1 < 10 ? '0' + (dd.getMonth() + 1) : dd.getMonth() + 1
  let d = dd.getDate() < 10 ? '0' + dd.getDate() : dd.getDate()
  let changedate = y + '-' + m + '-' + d
  return changedate
}

/**
 * 获取start(string),end(string)  之间的所有日期
 * format(yyyy-MM-dd) 返回的日期格式
 * */
export function getAllDate(start, end, format = 'yyyy-MM-dd') {
  let allDate = []
  if (!start) {
    return allDate
  }
  if (!end) {
    return allDate
  }
  let startDate = new Date(start)
  let endDate = new Date(end)
  let sd = new Date()
  sd.setUTCFullYear(
    startDate.getFullYear(),
    startDate.getMonth() + 1,
    startDate.getDate()
  )
  let ed = new Date()
  ed.setUTCFullYear(
    endDate.getFullYear(),
    endDate.getMonth() + 1,
    endDate.getDate()
  )
  let sdTime = sd.getTime()
  let edTime = ed.getTime()
  let onDay = 24 * 60 * 60 * 1000
  let stamp = sdTime
  for (; stamp <= edTime; ) {
    let dayTime = new Date(stamp)
    let dateStr = `${dayTime.getFullYear()}-${dayTime.getMonth()}-${dayTime.getDate()}`
    let date = dateFormat(new Date(dateStr), format)
    allDate.push(date)
    stamp += onDay
  }
  return allDate
}

/**
 * 获取start(string),end(string)  之间的所有月份
 * format(yyyy-MM) 返回的格式
 * */
export function getAllMonth(start, end, format = 'yyyy-MM') {
  let allMonth = []
  if (!start) {
    return allMonth
  }
  if (!end) {
    return allMonth
  }
  let startMonth = new Date(start)
  let endMonth = new Date(end)
  let sd = new Date()
  sd.setFullYear(startMonth.getFullYear(), startMonth.getMonth())
  let ed = new Date()
  ed.setFullYear(endMonth.getFullYear(), endMonth.getMonth())
  let cu = sd
  for (; cu <= ed; ) {
    let month = cu.getMonth()
    let date = dateFormat(cu, format)
    allMonth.push(date)
    cu.setMonth(month + 1)
  }
  return allMonth
}

/**
 * 获取start(string),end(string)  之间的所有年份
 * format(yyyy) 返回的格式
 * */
export function getAllYear(start, end, format = 'yyyy') {
  let allYear = []
  if (!start) {
    return allYear
  }
  if (!end) {
    return allYear
  }
  let startMonth = new Date(start)
  let endMonth = new Date(end)
  let sd = new Date()
  sd.setFullYear(startMonth.getFullYear(), startMonth.getMonth())
  let ed = new Date()
  ed.setFullYear(endMonth.getFullYear(), endMonth.getMonth())
  let cu = sd
  for (; cu <= ed; ) {
    let year = cu.getFullYear()
    let dateStr = `${year}年`
    allYear.push(dateStr)
    cu.setFullYear(year + 1)
  }
  return allYear
}

/**
 * 获取月天数
 * month 年月
 * */
export function getCountDays(month) {
  var curDate = month ? new Date(month + '-01') : new Date()
  // 获取当前月份
  var curMonth = curDate.getMonth()
  // 实际月份比curMonth大1，下面将月份设置为下一个月
  curDate.setMonth(curMonth + 1)
  // 将日期设置为0，表示自动计算为上个月（这里指的是当前月份）的最后一天
  curDate.setDate(0)
  // 返回当前月份的天数
  return curDate.getDate()
}
