import { ElMessageBox, ElMessage } from 'element-plus'

/**
 * 需要重写变量方法时直接在useCrud结构出的方法重写覆盖
 * importConfig: {
 *  nameKey: String 显示的name字段名 (用于删除提醒)
 *  idKey: String | Number id字段名
 * 	dateRangeKey: String  时间段字段
 *	startTimeKey:String  开始时间字段
 *	endTimeKey: String  结束时间字段
 *	isPage: Boolean 是否分页
 *	query: Object  默认传入搜索参数,不可清空
 *	defaultQuery:Object 默认传入搜索参数,可清空 {参数名称：值}
 * 	getListApi: Promise 查询列表接口
 *  saveApi: Promise 新增接口
 *  updateApi: Promise 编辑接口
 *  delApi: Promise 删除接口
 *  onLoadAfter: function() 加载完成后执行的方法
 * }
 * @param {*} importConfig
 * @returns
 */

export default function (importConfig) {
  const config = Object.assign(
    {
      nameKey: 'name', //	name 字段
      idKey: 'id', //id字段
      dateRangeKey: 'time', //时间段字段
      startTimeKey: 'stm', //开始时间字段
      endTimeKey: 'etm', //结束时间字段
      isPage: true, //是否分页
      query: {}, // 默认传入搜索参数,不可清空
      defaultQuery: {} // 默认传入搜索参数,可清空 {参数名称：值}
    },
    importConfig
  )

  const crudObj = reactive({
    page: {
      pageSize: 10,
      currentPage: 1,
      total: 0
    },
    query: importConfig.defaultQuery || {},
    tableLoading: false,
    data: [],
    form: {},
    selectionList: []
  })
  /**
   * 搜索列表数据
   * @param {Object}  page 分页
   * @param {Object}  params 参数
   * @param {Object}  defaultQuery 默认传入搜索参数,不可清空
   */
  const onLoad = (page, params, defaultQuery) => {
    if (defaultQuery) config.query = defaultQuery
    if (!config.getListApi) {
      console.error('请填入getListApi')
      return
    }
    //时间字段转换
    if (crudObj.query[config.dateRangeKey]?.length) {
      crudObj.query[config.startTimeKey] = crudObj.query[config.dateRangeKey][0]
      crudObj.query[config.endTimeKey] = crudObj.query[config.dateRangeKey][1]
      delete crudObj.query[config.dateRangeKey]
    }
    let paramsObj = {
      ...params,
      ...crudObj.query,
      ...config.query
    }
    if (config.isPage) {
      paramsObj.current = page.currentPage
      paramsObj.size = page.pageSize
    }

    crudObj.tableLoading = true
    config
      .getListApi(paramsObj)
      .then((res) => {
        if (res.success && res.data) {
          if (config.isPage) {
            //分页
            crudObj.data = res.data.records || []
            crudObj.page.total = res.data.total || 0
          } else crudObj.data = res.data || [] //不分页
        }
      })
      .finally(() => {
        crudObj.tableLoading = false
        if (typeof config?.onLoadAfter === 'function') {
          config.onLoadAfter(paramsObj)
        }
      })
  }

  /**
   * 删除
   */
  const rowDel = (row, index, done) => {
    if (!config.delApi) {
      console.error('请填入delApi')
      return
    }
    ElMessageBox.confirm(
      `确定删除${row[config.nameKey] ? '【' + row[config.nameKey] + '】' : '该数据'}吗？`,
      '温馨提示',
      {
        confirmButtonText: '确认',
        cancelButtonText: '取消',
        type: 'warning'
      }
    )
      .then(() => {
        crudObj.tableLoading = true
        return config.delApi({
          ids: row[config.idKey]
        })
      })
      .then(() => {
        crudObj.tableLoading = false
        ElMessage({
          type: 'success',
          message: '操作成功'
        })
        // 数据回调进行刷新
        done && done(row)
        refreshChange()
      })
  }

  /**
   * 新增
   */
  const rowSave = (row, done, loading) => {
    if (!config.saveApi) {
      console.error('请填入saveApi')
      return
    }
    row = Object.assign(row, config.query)
    crudObj.tableLoading = true
    config
      .saveApi(row)
      .then(
        (res) => {
          if (res.success) {
            ElMessage({
              message: '操作成功',
              type: 'success'
            })
            done && done(row)
            refreshChange()
          } else loading()
        },
        (error) => {
          window.console.log(error)
          loading()
        }
      )
      .finally(() => {
        crudObj.tableLoading = false
      })
  }

  /**
   * 编辑
   */
  const rowUpdate = (row, index, done, loading) => {
    if (!config.updateApi) {
      console.error('请填入updateApi')
      return
    }
    row = Object.assign(row, config.query)
    crudObj.tableLoading = true
    config
      .updateApi(row)
      .then(
        (res) => {
          if (res.success) {
            ElMessage({
              message: '操作成功',
              type: 'success'
            })
            done && done(row)
            refreshChange()
          } else loading()
        },
        (error) => {
          window.console.log(error)
          loading()
        }
      )
      .finally(() => {
        crudObj.tableLoading = false
      })
  }

  /**
   * 搜索按钮事件
   */
  const searchChange = (params, done) => {
    crudObj.query = params
    crudObj.page.currentPage = 1
    onLoad(crudObj.page, crudObj.params)
    done && done()
  }

  /**
   * 清空按钮事件
   */
  const searchReset = () => {
    crudObj.query = {}
    onLoad(crudObj.page)
  }

  // 分页
  const currentChange = (currentPage) => {
    crudObj.page.currentPage = currentPage
  }

  const sizeChange = (pageSize) => {
    crudObj.page.pageSize = pageSize
  }

  // 刷新
  const refreshChange = () => {
    onLoad(crudObj.page, crudObj.query)
  }

  //批量删除
  function selectionChange(list) {
    crudObj.selectionList = list
  }
  function handleDelete() {
    if (crudObj.selectionList.length === 0) {
      ElMessage.warning('请选择至少一条数据')
      return
    }
    ElMessageBox.confirm('确定将选择数据删除?', '温馨提示', {
      confirmButtonText: '确认',
      cancelButtonText: '取消',
      type: 'warning'
    })
      .then(() => {
        let ids = crudObj.selectionList.map((item) => item.id).join(',')
        config.delApi({ ids }).then(() => {
          ElMessage({
            type: 'success',
            message: '操作成功'
          })
          // 刷新表格数据并重载
          refreshChange()
        })
      })
      .catch(() => {})
  }

  return {
    searchChange,
    searchReset,
    currentChange,
    sizeChange,
    refreshChange,
    onLoad,
    rowDel,
    rowSave,
    rowUpdate,
    selectionChange,
    handleDelete,
    crudObj
  }
}
