/**
 * Created by ebi on 2017/5/11.
 */
import axios from 'axios'
// import request from './request'
import router from '@/router'
import { getToken, removeToken } from '@/util/auth'
import website from '@/config/website'
import { Base64 } from 'js-base64'
import { ElMessage, ElNotification } from 'element-plus'
import { refreshToken, isRefreshRequest } from './refreshToken'

// 读取环境变量
axios.defaults.baseURL = import.meta.env.VITE_BASE_URL

axios.interceptors.request.use(
  (config) => {
    const meta = config.meta || {}
    const isToken = meta.isToken === false

    if (getToken() && !isToken) {
      config.headers[website.tokenHeader] = 'bearer ' + getToken()
    }

    return config
  },
  (err) => {
    return Promise.reject(err)
  }
)

// insurance 保险 502 503 504时兜底的
function fetch(data) {
  let {
    url = '',
    params = {},
    method = 'get',
    contentType = 'form',
    headers = {},
    timeout = 60000
  } = data
  // console.log(data)
  let contentArr = {
    form: 'application/x-www-form-urlencoded',
    json: 'application/json',
    file: 'multipart/form-data'
  }
  return new Promise((resolve, reject) => {
    let requestParams = {
      timeout,
      method: method,
      url: url,
      headers: {
        'Tenant-Id': website.tenantId,
        'Content-Type': contentArr[contentType],
        Authorization: `Basic ${Base64.encode(`${website.clientId}:${website.clientSecret}`)}`,
        ...headers
      }
    }
    if (contentType === 'form') {
      requestParams.params = params
    } else {
      requestParams.data = params
    }
    axios(requestParams)
      .then((response) => {
        let { status, data = {}, statusText } = response
        if (status >= 200 && status < 400) {
          resolve(data)
        } else {
          ElMessage.error(status + '-' + statusText)
        }
      })
      .catch(async (err) => {
        if (
          /502|503|504/.test(err.message) ||
          (err + '').indexOf('timeout') > -1
        ) {
          let msg = '系统繁忙，正在为您排队中，请稍后再试'
          if (window.location.href.indexOf('/zu') > -1) {
            msg += ` ${err.message}`
          }
          ElMessage.error(msg)
          reject(new Error(msg))
        } else if (/401/.test(err.message) && !isRefreshRequest(err.config)) {
          // 401, token失效，无感刷新token
          const isSuccess = await refreshToken()
          // 重新请求
          if (isSuccess) {
            err.config.headers['Blade-Auth'] = `bearer ${getToken()}`
            const resp = await fetch(err.config)
            return resolve(resp)
          } else {
            // token没有获取到，需要登录
            removeToken()
            router.push({
              name: 'login',
              query: {
                redirect: encodeURIComponent(window.location.href)
              }
            })
            reject(new Error('需要登录'))
          }
        } else {
          const msg =
            err.response && err.response.data && err.response.data.msg
              ? err.response.data.msg
              : '网络异常，请点击重试'
          ElNotification({
            title: '提示',
            message: msg + '，请刷新重试',
            type: 'error'
          })
          reject(new Error(msg))
        }
      })
  })
}

export default fetch
