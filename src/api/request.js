import axios from 'axios'
import { getToken, removeToken } from '@/util/auth'
import { refreshToken, isRefreshRequest } from './refreshToken'
import { getStore } from '@/util/store'
import { Base64 } from 'js-base64'
import website from '@/config/website'
import { ElMessage, ElNotification } from 'element-plus'
import router from '@/router'

const tenantId = getStore({ name: 'tenantId' }) || website.tenantId

// 创建http实例
const http = axios.create({
  baseURL: '',
  timeout: 60000,
  headers: {
    'Tenant-Id': tenantId,
    'Content-Type': 'application/json; charset=utf-8',
    Authorization: `Basic ${Base64.encode(`${website.clientId}:${website.clientSecret}`)}`,
    'Blade-Auth': 'bearer ' + getToken()
  }
})

// 请求拦截
http.interceptors.request.use(async (config) => {
  const meta = config.meta || {}
  const isToken = meta.isToken === false

  if (getToken() && !isToken) {
    config.headers[website.tokenHeader] = 'bearer ' + getToken()
  }
  return config
})

// 响应拦截
http.interceptors.response.use(
  (res) => {
    return new Promise((resolve, reject) => {
      const { code, msg } = res.data
      if (code && code !== 200) {
        reject(res.data)
        return ElMessage.error(msg)
      }
      return resolve(res.data)
    })
  },
  async (err) => {
    if (/502|503|504/.test(err.message) || (err + '').indexOf('timeout') > -1) {
      let msg = '系统繁忙，正在为您排队中，请稍后再试'
      if (window.location.href.indexOf('/zu') > -1) {
        msg += ` ${err.message}`
      }
      ElMessage.error(msg)
    } else if (/401/.test(err.message) && !isRefreshRequest(err.config)) {
      // 401, token失效，无感刷新token
      const isSuccess = await refreshToken()
      // 重新请求
      if (isSuccess) {
        err.config.headers['Blade-Auth'] = `bearer ${getToken()}`
        const resp = await http(err.config)
        return resp
      } else {
        // token没有获取到，需要登录
        removeToken()
        router.push({
          name: 'login',
          query: {
            redirect: encodeURIComponent(window.location.href)
          }
        })
        Promise.reject(new Error('需要登录'))
      }
    } else {
      const msg =
        err.response && err.response.data && err.response.data.msg
          ? err.response.data.msg
          : '网络异常，请点击重试'
      ElNotification({
        title: '提示',
        message: msg + '，请刷新重试',
        type: 'error'
      })
      reject(err.response?.data.msg || '')
    }
  }
)

export default http
