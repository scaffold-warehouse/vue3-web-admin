/**
 * 系统管理
 */
import fetch from '@/api/fetch'

export default {
  // 机构管理
  getList(params) {
    return fetch({
      url: '/api/blade-system/dept/list',
      params
    })
  },
  getLazyList(params) {
    return fetch({
      url: '/api/blade-system/dept/lazy-list',
      params
    })
  },
  getDeptTree(params) {
    return fetch({
      url: '/api/blade-system/dept/tree',
      params
    })
  },
  getLazyTree(params) {
    return fetch({
      url: '/api/blade-system/dept/lazy-tree',
      params
    })
  },
  submit(params) {
    return fetch({
      url: '/api/blade-system/dept/submit',
      params,
      method: 'post',
      contentType: 'json'
    })
  },
  del(params) {
    return fetch({
      url: '/api/blade-system/dept/remove',
      params,
      method: 'post'
    })
  }
}
