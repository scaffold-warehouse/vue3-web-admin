import request from 'axios'

export const getLazyList = (parentId, params) => {
  return request({
    url: '/api/app-menu/lazy-list',
    method: 'get',
    params: {
      ...params,
      parentId
    }
  })
}

export const getLazyMenuList = (parentId, params) => {
  return request({
    url: '/api/app-menu/lazy-menu-list',
    method: 'get',
    params: {
      ...params,
      parentId
    }
  })
}

export const getMenuList = (current, size, params) => {
  return request({
    url: '/api/app-menu/menu-list',
    method: 'get',
    params: {
      ...params,
      current,
      size,
    }
  })
}

export const getMenuTree = (tenantId) => {
  return request({
    url: '/api/app-menu/tree',
    method: 'get',
    params: {
      tenantId,
    }
  })
}

export const remove = (ids) => {
  return request({
    url: '/api/app-menu/remove',
    method: 'post',
    params: {
      ids,
    }
  })
}

export const add = (row) => {
  return request({
    url: '/api/app-menu/submit',
    method: 'post',
    data: row
  })
}

export const update = (row) => {
  return request({
    url: '/api/app-menu/submit',
    method: 'post',
    data: row
  })
}

export const getMenu = (id) => {
  return request({
    url: '/api/app-menu/detail',
    method: 'get',
    params: {
      id,
    }
  })
}

export const getTopMenu = () => request({
  url: '/api/app-menu/top-menu',
  method: 'get'
})

export const getGrantTree = () => request({
  url: '/api/app-menu/grant-tree',
  method: 'get'
})

export const getRoutes = (topMenuId) => request({
  url: '/api/app-menu/routes',
  method: 'get',
  params: {
    topMenuId,
  }
})

export const getGrantRole = (roleIds) => {
  return request({
    url: '/api/app-menu/role-tree-keys',
    method: 'get',
    params: {
      roleIds,
    }
  })
}

export const phoneGrant = (roleIds, menuIds) => {
  return request({
    url: '/api/app-menu/grant',
    method: 'post',
    data: {
      roleIds,
      menuIds,
    }
  })
}