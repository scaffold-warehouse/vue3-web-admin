import request from 'axios'
import fetch from '../fetch'
import website from '@/config/website'

export default {
  menu: {
    getList(params) {
      return fetch({
        url: '/api/blade-system/menu/list',
        params
      })
    },
    getLazyList(params) {
      return fetch({
        url: '/api/blade-system/menu/lazy-list',
        params
      })
    },
    submit(params) {
      return fetch({
        url: '/api/blade-system/menu/submit',
        params,
        method: 'post',
        contentType: 'json'
      })
    },
    del(params) {
      return fetch({
        url: '/api/blade-system/menu/remove',
        params,
        method: 'post'
      })
    },
    getMenu(params) {
      return fetch({
        url: '/api/blade-system/menu/detail',
        params
      })
    },
    getMenuTree(params) {
      return fetch({
        url: '/api/blade-system/menu/tree',
        params: {
          tenantId: website.tenantId
        }
      })
    }
  }
}

export const getLazyMenuList = (parentId, params) => {
  return request({
    url: '/api/blade-system/menu/lazy-menu-list',
    method: 'get',
    params: {
      ...params,
      parentId
    }
  })
}

export const getMenuList = (current, size, params) => {
  return request({
    url: '/api/blade-system/menu/menu-list',
    method: 'get',
    params: {
      ...params,
      current,
      size
    }
  })
}

export const getTopMenu = () =>
  request({
    url: '/api/blade-system/menu/top-menu',
    method: 'get'
  })

export const getRoutes = (topMenuId) =>
  request({
    url: '/api/blade-system/menu/routes',
    method: 'get',
    params: {
      topMenuId
    }
  })
