import fetch from '@/api/fetch'
import request from 'axios'
// import http from '@/api/request'

export default {
  user: {
    getList(params) {
      return fetch({
        url: '/api/blade-user/page',
        params
      })
    },
    del(params) {
      return fetch({
        url: '/api/blade-user/remove',
        params,
        method: 'post'
      })
    },
    submit(params) {
      return fetch({
        url: '/api/blade-user/submit',
        params,
        method: 'post',
        contentType: 'json'
      })
    },
    update(params) {
      return fetch({
        url: '/api/blade-user/update',
        params,
        method: 'post',
        contentType: 'json'
      })
    }
  }
}

export const updatePlatform = (userId, userType, userExt) => {
  return request({
    url: '/api/blade-user/update-platform',
    method: 'post',
    params: {
      userId,
      userType,
      userExt
    }
  })
}

export const getUser = (id) => {
  return request({
    url: '/api/blade-user/detail',
    method: 'get',
    params: {
      id
    }
  })
}

export const getUserPlatform = (id) => {
  return request({
    url: '/api/blade-user/platform-detail',
    method: 'get',
    params: {
      id
    }
  })
}

export const getUserInfo = () => {
  return request({
    url: '/api/blade-user/info',
    method: 'get'
  })
}

export const resetPassword = (userIds) => {
  return request({
    url: '/api/blade-user/reset-password',
    method: 'post',
    params: {
      userIds
    }
  })
}

export const updatePassword = (oldPassword, newPassword, newPassword1) => {
  return fetch({
    url: '/api/blade-user/update-password',
    method: 'post',
    params: {
      oldPassword,
      newPassword,
      newPassword1
    }
  })
}

export const updateInfo = (row) => {
  return request({
    url: '/api/blade-user/update-info',
    method: 'post',
    data: row
  })
}

export const grant = (userIds, roleIds) => {
  return request({
    url: '/api/blade-user/grant',
    method: 'post',
    params: {
      userIds,
      roleIds
    }
  })
}
