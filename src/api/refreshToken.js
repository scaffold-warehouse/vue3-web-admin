import axios from 'axios'
import website from '@/config/website'
import { Base64 } from 'js-base64'
import { getStore } from '@/util/store'
import { setToken, setRefreshToken, getRefreshToken } from '@/util/auth'

const tenantId = getStore({ name: 'tenantId' }) || website.tenantId
// const router = useRouter()

let promise = null

export async function refreshToken() {
  if (promise) {
    return promise
  }
  promise = new Promise(async (resolve) => {
    console.log('刷新token')
    const resp = await axios({
      url: '/api/blade-auth/oauth/token',
      method: 'POST',
      headers: {
        'Tenant-Id': tenantId,
        'Content-Type': 'application/x-www-form-urlencoded',
        Authorization: `Basic ${Base64.encode(`${website.clientId}:${website.clientSecret}`)}`
      },
      data: {
        refresh_token: getRefreshToken(),
        tenantId: tenantId,
        grant_type: 'refresh_token',
        scope: 'all'
      },
      __isRefreshToken: true
    })

    if (resp.data.access_token) {
      setToken(resp.data.access_token)
      setRefreshToken(resp.data.refresh_token)
      resolve(true)
    } else {
      // router.push({ name: 'login' })
      console.log('token过期')
      refreshToken()
      resolve(false)
    }
  })

  promise.finally(() => {
    promise = null
  })
  return promise
}

export function isRefreshRequest(config) {
  return !!config.__isRefreshToken
}
