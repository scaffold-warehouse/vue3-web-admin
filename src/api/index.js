/**
 * Created by ebi on 2017/5/11.
 */
import fetch from './fetch'

const dicPath = '/api/blade-system/dict-biz/dictionary?code=' // 业务字典
const dicTreePath = '/api/blade-system/dict-biz/dictionary-tree?code=' // 业务字典树
const sysPath = '/api/blade-system/dict/dictionary?code=' // 系统字典
const treePath = '/api/blade-system/dict/dictionary-tree?code=' // 系统树字典
const uploadPath = '/api/blade-resource/oss/endpoint/put-file-attach' // 上传接口

export default {
  dicPath,
  uploadPath: uploadPath, // 上传
  common: {
    chinaTree: '/api/blade-system/region/lazy-tree', // 全国树
    regionTree: '/api/blade-system/region/area-tree' // 所属区域
  },
  getRegion(params) {
    // 获取行政区划
    return fetch({
      url: this.common.regionTree,
      params
    })
  },
  getDictData(url, params) {
    // 获取下拉菜单
    return fetch({
      url,
      params
    })
  },
  getDicKey(key) {
    // 获取字典列表
    return fetch({
      url: dicPath + key
    })
  },
  getDicTreeKey(key) {
    // 获取字典树
    return fetch({
      url: dicTreePath + key
    })
  },
  getDicPath(key) {
    // 获取字典下拉
    return dicPath + key
  },
  getUserList(params) {
    // 获取所有用户列表
    return fetch({
      url: '/api/blade-user/user-list',
      params
    })
  },
  download(id, name) {
    // 下载附件
    return download('/api/blade-resource/oss/endpoint/downFile/' + id, name)
  },
  exportFile(url, name, params) {
    // 导出附件
    return download(url, name, params)
  }
}
