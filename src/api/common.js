/**
 * 基础配置
 */

import fetch from './fetch'

export default {
  /*
   * 登录接口
   */
  login: {
    getCaptcha() {
      // 图片验证码
      return fetch({
        url: '/api/blade-auth/oauth/captcha'
      })
    },
    loginByUsername(params, headers) {
      // 登录接口
      return fetch({
        url: '/api/blade-auth/oauth/token',
        params,
        method: 'post',
        headers
      })
    },
    refreshToken(refresh_token, tenantId) {
      // 刷新验证码
      return fetch({
        url: '/api/blade-auth/oauth/token',
        params: {
          refresh_token,
          tenantId,
          grant_type: 'refresh_token',
          scope: 'all'
        },
        method: 'post'
      })
    }
  },
  permission: {
    getButtons() {
      // 获取所有按钮菜单
      return fetch({
        url: '/api/blade-system/menu/buttons'
      })
    },
    getRoutes() {
      // 获取所有路由
      return fetch({
        url: '/api/blade-system/menu/routes'
      })
    },
    getDicTreeAll() {
      // 获取所有字典
      return fetch({
        url: '/api/blade-system/dictData/dictionary-tree'
      })
    }
  },
  config: {
    // getInfo(params) {
    // 	return fetch({
    // 		url: '/api/mdgq/sys-init-config',
    // 		params
    // 	})
    // },
    getDicPath(code) {
      return '/api/blade-system/dict-biz/dictionary?code=' + code
    }
  },
  data: {
    getAllStation(params) {
      return fetch({
        url: '/api/station/selectList',
        params
      })
    }
  }
}
