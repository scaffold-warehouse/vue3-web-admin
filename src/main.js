import { createApp } from 'vue'

import App from './App.vue'
import router from './router'
import store from './store'
import axios from 'axios'
import WujieVue from 'wujie-vue3'

// 组件库
import ElementPlus from 'element-plus'
import 'element-plus/dist/index.css'
import 'element-plus/theme-chalk/dark/css-vars.css'

// 样式
import './assets/css/tailwind.css'
import Avue from '@smallwei/avue'
import '@smallwei/avue/lib/index.css'
import './assets/css/main.scss'

const app = createApp(App)
app.use(store).use(router).use(ElementPlus).use(WujieVue).use(Avue, { axios })
app.mount('#app')

// element 图标库
import * as ElementPlusIconsVue from '@element-plus/icons-vue'
Object.keys(ElementPlusIconsVue).forEach((key) => {
  app.component(key, ElementPlusIconsVue[key])
})

// 无界子应用
// const { setupApp, preloadApp } = WujieVue
// setupApp({
//   name: 'wujie-vue',
//   el: '#wujie-vue',
//   sync: true
// })
