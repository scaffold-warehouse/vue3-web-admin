const env = process.env.NODE_ENV

const config = {
  development: {
    baseUrl: ''
  },
  production: {
    baseUrl: ''
  }
}

export default config[env]
