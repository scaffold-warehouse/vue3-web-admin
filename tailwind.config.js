/** @type {import('tailwindcss').Config} */
module.exports = {
  darkMode: 'class',
  content: ['./src/**/*.{html,vue}'],
  theme: {
    extend: {}
  },
  plugins: []
}
